//filename of the script that should be executed in PhantomJS
var authScriptName = 'phantom-auth.js';

//getting path to PhantomJS executable
var phantomjs = require('phantomjs').path,
    path = require('path'),
    execFile = require('child_process').execFile,
    util = require("util"),
    EventEmitter = require("events").EventEmitter,
//getting full path to auth script
    authScriptPath = path.join(__dirname, authScriptName);

var Auth = function (appId, permissions) {
    if (arguments.length !== 2) {
        throw new Error('Incorrect arguments passed');
    }

    //app permissions
    var applicationScope;

    //checking received permissions type
    if (permissions instanceof Array) {
        applicationScope = permissions.join(',');
    } else {
        applicationScope = permissions;
    }

    //function for generating command args for executing phantomJS script
    var generateExecArgs = function (login, password) {
        var
            escapedLogin = encodeURIComponent(login),
            escapedPass = encodeURIComponent(password),
            escapedClientId = encodeURIComponent(appId),
            escapedAppScope = encodeURIComponent(applicationScope);
        return [
            authScriptPath,
            escapedLogin,
            escapedPass,
            escapedClientId,
            escapedAppScope
        ];
    };

    var Authorizer = function () {
        var _this = this;

        //inherit Authorizer from EventEmitter
        EventEmitter.call(this);

        //method for authorizing user in VK and getting token
        this.authorize = function (login, password, callback) {
            var args = generateExecArgs(login, password);
            //executing command in shell
            execFile(phantomjs, args, function (err, stdout, stderr) {
                //executing callback with error if shell error happened or PhantomJS was unable to process script
                if (err) {
                    if (callback) {
                        callback(err);
                    }
                    return _this.emit('error', err);
                }
                //returning authorization errors same way as shell error
                if (stderr.toString().length > 0) {
                    var phantomErr = new Error(stderr.toString());
                    if (callback) {
                        callback(phantomErr);
                    }
                    return _this.emit('error', phantomErr);
                }
                //splitting URL with token and retrieving token parameters
                var splitedURL = stdout.toString().split("#");
                var params = splitedURL[1];
                var paramsArray = params.split("&");
                var tokenParams = {};
                for (var i = 0, length = paramsArray.length; i < length; i++) {
                    var splitedParam = paramsArray[i].split("=");
                    tokenParams[splitedParam[0]] = splitedParam[1];
                }
                if (callback) {
                    callback(null, tokenParams);
                }
                return _this.emit('auth', tokenParams);
            });
        }
    };
    //inherit Authorizer from EventEmitter
    util.inherits(Authorizer, EventEmitter);

    return new Authorizer();
};

module.exports = Auth;
